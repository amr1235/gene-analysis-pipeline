# Use pacman to load add-on packages as desired
pacman::p_load(pacman,relimp) 

f <- file("stdin") # get the data from the python code

genes_data <- read.table(f, sep =",", header = TRUE, stringsAsFactors = FALSE) # convert into df
showData(genes_data) #show the data in pop up window

X11( title = "Mass plot") # popup window for the plot
plot(genes_data$orig_mass, genes_data$delta_mass,   # plot the data
     pch = 19,         # Solid circle
     col = "#0000FF",  # Blue
     main = "Original mass vs Absolute mass change",
     xlab = "Mass",
     ylab = "Delta")
while(names(dev.cur()) !='null device') Sys.sleep(1) # loop to keep the window open