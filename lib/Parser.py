from Bio import SeqIO
import allel
from gtfparse import read_gtf
class Parser:
    
    @staticmethod
    def vcf(filename,chromosome) :
        df = allel.vcf_to_dataframe(filename)
        df = df[(df["CHROM"] == chromosome ) & (df["REF"].str.len() == 1) & (df["ALT_1"].str.len() == 1) & (df["ALT_2"].isnull()) & (df["ALT_3"].isnull()) ] # filter the dataframe to select the desired Chromosome and only point mutation
        return df[["POS","ALT_1"]]
    
    @staticmethod
    def gtf(filename,chromosome):    
        df = read_gtf(filename)
        return df[df["seqname"] == "chr"+chromosome]

    @staticmethod
    def fasta(filename):
        seq_import = SeqIO.parse(filename,'fasta')
        # Generator object
        return next(seq_import.records).seq