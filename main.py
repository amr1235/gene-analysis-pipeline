import logging
logging.basicConfig(filename='app.log', level=logging.INFO,filemode="w")
logging.captureWarnings(True)

from lib import *
import sys
import os
import concurrent.futures
import time

if __name__ == "__main__":

    # check run.sh arguments
    try : 
        f = open(sys.argv[2])
    except : 
        raise Exception("Please check your fasta file")
    
    if not(int(sys.argv[1]) in range(1,23) or sys.argv[1] == "X" or sys.argv[1] == "Y") : 
        raise Exception("please check your chromosome number")
    
    CHROMOSOME = sys.argv[1]
    
    vcf = os.path.join("data","mutations.vcf")
    fasta = sys.argv[2]
    gtf = os.path.join("data","geneannotations.gtf")

    start = time.perf_counter()
    # FASTA and GTF combined are read quicker than VCF.  
    # So read VCF in a separate PROCESS! 
    with concurrent.futures.ProcessPoolExecutor() as executor:
        vcf_future = executor.submit(Parser.vcf,vcf,CHROMOSOME)
        sequence = Parser.fasta(fasta)
        annotations = Parser.gtf(gtf,CHROMOSOME)
        if concurrent.futures.ALL_COMPLETED:
            chr = Chromosome(sequence,annotations)
            mutations = chr.mutate(vcf_future.result())
            results = chr.compare_masses(mutations)
    
    end = time.perf_counter()
    logging.info(f"Finished in {end  - start} seconds")
    sys.stdout.write(results.to_csv(index = False, header=True))
    sys.stdout.flush()